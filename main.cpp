#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

//Citire Matrice de incidenta din fisier text
void read_incidenta(int mat[100][100], int &n, int &m)
{
    puts("Matrice de incidenta:");
    ifstream x;
    x.open("mat_incidenta.txt");
    x >> n; //Numarul de noduri
    x >> m; //Numarul de arce
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            x >> mat[i][j];
    for  (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            cout << right << setw(4) << mat[i][j];
        cout << "\n";
    }
    puts("-------------------------------");
}

//Conversie matrice incidenta -> matrice adiacenta
void incidenta_to_adiacenta(int mat[100][100], int n, int m)
{
    puts("\nMatrice de adiacenta");
    int adiac[100][100] = {0};
    for (int j = 0; j < m; j++)
    {
        int k, l;
        for (int i = 0; i < n; i++)
        {
            if (mat[i][j] == 1)
                k = i; //Nodul sursa
            else if (mat[i][j] == -1)
                l = i; //Nodul destinatie
        }
        adiac[k][l] = 1;
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
            cout << right << setw(4) << adiac[i][j];
        cout << "\n";
    }
    puts("-------------------------------");
}

//Conversie matrice incidenta -> lista arce
void incidenta_to_lista_arce(int mat[100][100], int n, int m)
{
    puts("\nLista arce:");
    int l_arce[2][m];
    int k = 0;
    for (int j = 0; j < m; j++)
    {
        for (int i = 0; i < m; i++)
        {
            if (mat[i][j] == 1)
                l_arce[0][k] = i; //Nodul sursa
            if (mat[i][j] == -1)
                l_arce[1][k] = i; //Nodul destinatie
        }
        k++;
    }
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cout << right << setw(4) << l_arce[i][j];
        }
        cout << "\n";
    }
    puts("-------------------------------");
}

//Conersie matrice incidenta -> lista succesori
void incidenta_to_lista_succ(int mat [100][100], int n, int m)
{
    puts("\nLista succesori");
    int gr_ext[n] = {0};
    int filled = 0;
    int poz[n+1];
    poz[0] = 0;
    poz[n] = m;
    int succ[m];
    int k = 0;
    for (int j = 0; j < m; j++)
        for (int i = 0; i < n; i++)
        {
            if (mat[i][j] == 1)
                gr_ext[i]++; //Se calculeaza gradul exterior al fiecarui nod
        }
    for (int l = 0; l < n; l++) 
    {
        for (int i = 0; i < n; i++)                         //Matricea de incidenta este parcursa
        {                                                   //de n-ori, cautand la fiecare pas 'l'
            for (int j = 0; j < m; j++)                     //succesorii nodului 'l' 
            {
                if (mat[i][j] == -1 && mat[l][j] == 1)
                {
                    succ[k] = i;
                    k++;
                }
            }
        }
    }
    for (int i = 1; i < n; i++)
    {
        poz[i] = gr_ext[i-1] + filled;
        filled += gr_ext[i-1];
    }
    cout << "nod:";
    for (int i = 0; i < n; i++)
    {
        cout << right << setw(4) << i;
    }
    cout << "\n";
    cout << "poz:";
    for (int i = 0; i <= n; i++)
    {
        cout << right << setw(4) << poz[i];
    }
    cout << "\n";
    cout << "suc:";
    for (int i = 0; i < m; i++)
    {
        cout << right << setw(4) << succ[i];
    }
    cout << "\n";
    puts("-------------------------------");
}

//Conersie matrice incidenta -> lista predecesori
void incidenta_to_lista_pred(int mat[100][100], int n, int m)
{
    puts("\nLista predecesorilor");
    int gr_int[n] = {0};
    int filled = 0;
    int nodes[n+1];
    int pred[m];
    nodes[0] = 0;
    nodes[n] = m;
    int k = 0;
    for (int j = 0; j < m; j++)
        for (int i = 0; i < n; i++)
        {
            if (mat[i][j] == -1)
                gr_int[i]++;                           //Se calculeaza gradul interior al fiecarui nod
        }
    for (int l = 0; l < m; l++)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                if (mat[i][j] == 1 && mat[l][j] == -1) //Matricea de incidenta este parcursa
                {                                      //de n-ori, cautand la fiecare pas 'l'
                    pred[k] = i;                       //predecesorii nodului 'l'
                    k++;
                }
            }
        }
    }
    for (int i = 1; i < n; i++)
    {
        nodes[i] = gr_int[i-1] + filled;
        filled += gr_int[i-1];
    }
    cout << "nod:";
    for (int i = 0; i < n; i++)
    {
        cout << right << setw(4) << i;
    }
    cout << "\n";
    cout << "poz:";
    for (int i = 0; i <= n; i++)
    {
        cout << right << setw(4) << nodes[i];
    }
    cout << "\n";
    cout << "pre:";
    for (int i = 0; i < m; i++)
    {
        cout << right << setw(4) << pred[i];
    }
    cout << "\n";
    puts("-------------------------------");
}

//Citire lista succesori din fisier text
void read_lista_succ(int poz[100], int succ[100], int &n, int &m)
{
    puts("\nLista succesori:");
    ifstream x;
    x.open("lista_succ.txt");
    x >> n;
    x >> m;
    for (int i = 0; i <= n; i++)
        x >> poz[i];
    for (int i = 0; i < m; i++)
        x >> succ[i];
    cout << "\nnod:";
    for (int i = 0; i< n; i++)
        cout << right << setw(4) << i;
    cout << "\npoz:";
    for (int i = 0; i <= n; i++)
        cout << right << setw(4) << poz[i];
    cout << "\nsuc:";
    for (int i = 0; i < m; i++)
        cout << right << setw(4) << succ[i];
    cout << "\n";
    puts("-------------------------------");
}

//Conversie lista succesori -> matrice de adiacenta
void lista_succ_to_adiacenta(int poz[100], int succ[100], int n, int m)
{
    puts("\nMatrice de adiacenta:");
    int tab[n][n];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            tab[i][j] = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = poz[i]; j < poz[i + 1]; j++)
        {
            tab[i][succ[j]] = 1;
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
            cout << right << setw(4) << tab[i][j];
        cout << "\n";
    }
    puts("-------------------------------");
}

//Conversie lista succesori -> matrice de incidenta
void lista_succ_to_incidenta(int poz[100], int succ[100], int n, int m)
{
    puts("\nMatrice de incidenta:");
    int tab[n][m];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            tab[i][j] = 0;

    int k = 0;
    for (int i = 0; i < n; i++)
        for (int j = poz[i]; j < poz[i+1]; j++)
        {
            tab[i][k] = 1;
            tab[succ[j]][k] = -1;
            k++;
        }
    for  (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            cout << right << setw(4) << tab[i][j];
        cout << "\n";
    }
    puts("-------------------------------");
}

//Conversi lista succesori -> lista arce
void lista_succ_to_lista_arce(int poz[100], int succ[100], int n, int m)
{
    puts("\nLista arce:");
    int tab[2][m];
    int k = 0;
    for (int i = 0; i < n; i++)
        for (int j = poz[i]; j < poz[i+1]; j++)
        {
            tab[0][k] = i;
            tab[1][k] = succ[j];
            k++;
        }
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cout << right << setw(4) << tab[i][j];
        }
        cout << "\n";
    }
    puts("-------------------------------");
}

//Conversie lista succesori -> lista predecesori
void lista_succ_to_lista_pred(int poz_succ[100], int succ[100], int n, int m)
{
    puts("\nLista predecesori: ");
    int gr_int[n + 1] = {0};
    int poz_pred[n+1];
    int pred[m];
    poz_pred[0] = 0;
    poz_pred[n] = m;
    int k = 0;
    for (int i = 0; i < m; i++)
    {
        gr_int[succ[i]]++;                      //Se calculeaza gradul interior al fiecarui nod   
    }
    for (int i = 1; i < n; i++)
    {                                                     //Se creeaza lista pozitiilor nodurilor
        poz_pred[i] = poz_pred[i - 1] + gr_int[i - 1];    //Pozitia de inceput succesorilor lui i este gradul 
    }                                                     //nodului i + pozitia de inceput a succ. nod. i-1                                            
    for (int l = 0; l < n; l++)
    {
        for (int i = 0; i < m; i++)
        {
            if (succ[i] == l)                              //Se parcurge lista succesorilor de n ori
            {                                              //La fiecare pas l se cauta predecesorii 
                for (int j = 0; j < n; j++)                //nodului l. Daca in lista de succ se gaseste
                {                                          //nodul l, se cauta predecesorul acestuia in  
                    if (poz_succ[j] <= i && i < poz_succ[j+1])       //in lista de pozitii
                    {
                        pred[k] = j;
                        k++;
                        break;
                    }
                }
            }
        }
    }
    cout << "\nnod:";
    for (int i = 0; i< n; i++)
        cout << right << setw(4) << i;
    cout << "\npoz:";
    for (int i = 0; i <= n; i++)
    {
        cout << right << setw(4) << poz_pred[i];
    }
    cout << "\npre:";
    for (int i = 0; i < m; i++)
    {
        cout << right << setw(4) << pred[i];
    }
    puts("\n-------------------------------");
}

//Citire matrice de adiacenta
void read_adiacenta(int tab[100][100], int &n, int &m)
    {
        puts("\nMatrice de adiacenta: ");
        m=0;
        ifstream x;
        x.open("mat_adiacenta.txt");
        x >> n;
        for(int i=0; i<n; i++)
            for(int j=0; j<n; j++)
        {
            x >> tab[i][j];
        }
        for(int i=0; i<n; i++)
        {
             for(int j=0; j<n; j++)
             {
                cout<< right << setw(4)<< tab[i][j];
                cout<<" ";
                if(tab[i][j]==1)
                    m++;
             }
             cout<<"\n";
        }
        puts("-------------------------------");
    }

//Conversie matrice adiacenta -> matrice incidenta
void adiacenta_to_incidenta(int mat[100][100], int n, int m)
    {
        puts("\nMatrice de incidenta");
        cout << "\n";
        int incid[n][m];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                incid[i][j] = 0;
        int k=0;
        for(int i = 0; i<n; i++)
            for(int j = 0; j<n; j++)
        {
            if(mat[i][j] == 1)
            {
                incid[i][k]=1;
                incid[j][k]=-1;
                k++;
            }
        }
        for(int i=0; i<n; i++)
        {
            for(int j=0; j<m; j++)
        {
            cout<< right << setw(4)<< incid[i][j];
            cout<<" ";
            }
            cout<<"\n";
        }
        puts("-------------------------------");
    }

//Conversie matrice adiacenta -> lista arce
void adiacenta_to_lista_arce(int mat[100][100], int n, int m)
    {
        puts("\nLista arce");
        cout << "\n";
        int l_arce[2][m];
        int k=0;
        for(int i=0; i<n; i++)
            for(int j=0; j<n; j++)
            {
                if(mat[i][j]==1)
                {
                    l_arce[0][k]=i;
                    l_arce[1][k]=j;
                    k++;
                }
            }
        for(int i=0; i<2; i++)
        {
             for(int j=0; j<m; j++)
             {
                cout<<right<<setw(4)<< l_arce[i][j];
             }
             cout<<"\n";
        }
        puts("-------------------------------");
    }

//Conversie matrice adiacenta -> lista succesori
void adiacenta_to_lista_succ(int mat[100][100], int n, int m)
    {
        puts("\nLista succesori:");
        cout << "\n";
        int poz[n+1];
        int succ[m];
        int k=0, filled =0, gr_ext=0;
        poz[0] = 0;
        for(int i=0; i<n; i++)
        {
            gr_ext=0;
            for(int j=0; j<n; j++)
            {
                if(mat[i][j]==1)
                {
                    gr_ext++;                 //La momentul gasirii unui 1 in mat de adiacenta,
                    succ[k]=j;                //se incrementeaza gradul exterior al nodului si 
                    k++;                      //succesorul lui este adaugat in lista de succesori 
                }
            }
            filled+=gr_ext;                   
            poz[i+1]=filled;                    
        }
        cout<<"nod:";
        for(int i=0; i<n; i++)
            cout<<right<<setw(4)<<i<<" ";
        cout<<"\n";
        cout<<"poz:";
        for(int i=0; i<n+1; i++)
            cout<<right<<setw(4)<<poz[i]<<" ";
        cout<<"\n";
        cout<<"suc:";
        for(int i=0; i<m; i++)
            cout<<right<<setw(4)<<succ[i]<<" ";
        cout<<"\n";
        puts("-------------------------------");
    }

//Conversie matrice adiacenta -> lista predecesori
void adiacenta_to_lista_pred(int mat[100][100], int n, int m)
    {
        puts("\nLista predecesori:");
        cout << "\n";
        int poz[n+1];
        int pred[m];
        int k=0, filled =0, gr_ext=0;
        poz[0] = 0;
        for(int i=0; i<n; i++)
        {
            gr_ext=0;
            for(int j=0; j<n; j++)
            {
                if(mat[j][i]==1)
                {
                    gr_ext++;               //Matricea de adiacenta este parcursa pe coloane, daca
                    pred[k]=j;              //se gaseste valoare 1, gradul exterior este incrementat
                    k++;                    //si nodul sursa este adaugat in lista de predecesori
                }
            }
            filled+=gr_ext;
            poz[i+1]=filled;
        }
        cout<<"nod:";
        for(int i=0; i<n; i++)
            cout<<right<<setw(4)<<i<<" ";
        cout<<"\n";
        cout<<"poz:";
        for(int i=0; i<n+1; i++)
            cout<<right<<setw(4)<<poz[i]<<" ";
        cout<<"\n";
        cout<<"suc:";
        for(int i=0; i<m; i++)
            cout<<right<<setw(4)<<pred[i]<<" ";
        cout<<"\n";
        puts("-------------------------------");
    }

//Citire lista arce din fisier text
void read_lista_arce(int mat[2][100], int &n, int &m)
{
    puts("\nLista arce:");
    ifstream x;
    x.open("lista_arce.txt");
    x >> n;
    x >> m;
    for(int i=0; i<2; i++)
        for(int j=0; j<m; j++)
    {
        x >> mat[i][j];
    }
    for(int i=0; i<2; i++)
        {
            for(int j=0; j<m; j++)
            {
                cout<< right << setw(4)<< mat[i][j];
                cout<<" ";
            }
            cout<<"\n";
        }
        puts("-------------------------------");
    }

//Conversie lsita arce -> matrice de adiacenta
void lista_arce_to_adiacenta(int mat[2][100], int n, int m)
{
    puts("\nMatrice de adiacenta:");
    int adiac[n][n];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            adiac[i][j] = 0;
    for(int j=0; j<m; j++)
    {
        adiac[mat[0][j]][mat[1][j]] = 1;
    }
    for(int i=0; i<n; i++)
        {
             for(int j=0; j<n; j++)
             {
                cout<< right << setw(4)<< adiac[i][j];
                cout<<" ";
             }
             cout<<"\n";
        }
    puts("-------------------------------");
}

//Conversie lista arce -> matrice de incidenta
void lista_arce_to_incidenta(int mat[2][100], int n, int m)
{
    puts("\nMatrtice de incidenta:");
    cout << "\n";
    int incid[n][m];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            incid[i][j] = 0;
    for(int j=0; j<m; j++)
    {
        incid[mat[0][j]][j]=1;
        incid[mat[1][j]][j]=-1;
    }
     for(int i=0; i<n; i++)
        {
             for(int j=0; j<m; j++)
             {
                cout<< right << setw(4)<< incid[i][j];
                cout<<" ";
             }
             cout<<"\n";
        }
    puts("-------------------------------");
}

//Conversie lista arce -> lista succesori
void lista_arce_to_lista_succ(int mat[2][100], int n, int m)
{
    puts("\nLista succesori:");
    int poz[n+1];
    int gr_ext[n] = {0};
    poz[0]=0;
    poz[n]=m;
    int succ[m];
    int k = 0;
    int cur_nod = 0;
    for (int j = 0; j < m; j++)
    {
        gr_ext[mat[0][j]]++;
    }
    for (int j = 1; j < n; j++)
    {
        poz[j] = poz[j-1] + gr_ext[j-1];
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (mat[0][j] == i)
            {
                succ[k] = mat[1][j];       
                k++;
            }
        }
    }
    cout << "\n" ;
    cout << "nod:";
    for (int i = 0; i < n;i++)
    {
        cout<< right << setw(4)<< i;
                cout<<" ";
    }
    cout << "\n" ;
    cout << "poz:";
    for (int i = 0; i <= n;i++)
    {
        cout<< right << setw(4)<< poz[i];
                cout<<" ";
    }
    cout << "\n" ;
    cout << "suc:";
    for (int i = 0; i < m;i++)
    {
        cout<< right << setw(4)<< succ[i];
                cout<<" ";
    }
    cout << "\n";
    puts("-------------------------------");
}

//Conversie lista arce -> lista predecesori
void lista_arce_to_lista_pred(int mat[2][100], int n, int m)
{
    puts("\nLista predecesori:");
    int poz[n+1];
    int grad_int[n] = {0};
    poz[0]=0;
    poz[n]=m;
    int pred[m];
    int k = 0;
    int cur_nod = 0;
    for (int j = 0; j < m; j++)
    {
        grad_int[mat[1][j]]++;
    }
    for (int j = 1; j < n; j++)
    {
        poz[j] = poz[j-1] + grad_int[j-1];
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (mat[1][j] == i)
            {
                pred[k] = mat[0][j];
                k++;
            }
        }
    }
    cout << "\n" ;
    cout << "nod:";
    for (int i = 0; i < n;i++)
    {
        cout<< right << setw(4)<< i;
                cout<<" ";
    }
    cout << "\n" ;
    cout << "poz:";
    for (int i = 0; i <= n;i++)
    {
        cout<< right << setw(4)<< poz[i];
                cout<<" ";
    }
    cout << "\n" ;
    cout << "pre:";
    for (int i = 0; i < m;i++)
    {
        cout<< right << setw(4)<< pred[i];
                cout<<" ";
    }
    cout << "\n";
    puts("-------------------------------");
}

//Citire lista predecesori din fisier text
void read_lista_pred(int poz[100], int pred[100], int &n, int &m)
{
    puts("\nLista predecesori:");
    ifstream x;
    x.open("lista_pred.txt");
    x >> n;
    x >> m;
    for (int i = 0; i <= n; i++)
        x >> poz[i];
    for (int i = 0; i < m; i++)
        x >> pred[i];
    cout << "\nnod:";
    for (int i = 0; i< n; i++)
        cout << right << setw(4) << i;
    cout << "\npoz:";
    for (int i = 0; i <= n; i++)
        cout << right << setw(4) << poz[i];
    cout << "\npre:";
    for (int i = 0; i < m; i++)
        cout << right << setw(4) << pred[i];
    cout << "\n";
    puts("-------------------------------");
}

//Conversie lista predecesori -> matrice de adiacenta
void lista_pred_to_adiacenta(int poz[100], int pred[100], int n, int m)
{
    puts("\nMatrice de adiacenta:");
    int tab[n][n];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            tab[i][j] = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = poz[i]; j < poz[i + 1]; j++)
        {
            tab[pred[j]][i] = 1;
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
            cout << right << setw(4) << tab[i][j];
        cout << "\n";
    }
    puts("-------------------------------");
}

//Conversie lista predecesori -> matrice de incidenta
void lista_pred_to_incidenta(int poz[100], int pred[100], int n, int m)
{
    puts("\nMatrice de incidenta:");
    int tab[n][m];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            tab[i][j] = 0;

    int k = 0;
    for (int i = 0; i < n; i++)
        for (int j = poz[i]; j < poz[i+1]; j++)
        {
            tab[i][k] = -1;
            tab[pred[j]][k] = 1;
            k++;
        }
    for  (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            cout << right << setw(4) << tab[i][j];
        cout << "\n";
    }
    puts("-------------------------------");
}

//Conversi lista predecesori -> lista arce
void lista_pred_to_lista_arce(int poz[100], int pred[100], int n, int m)
{
    puts("\nLista arce:");
    int tab[2][m];
    int k = 0;
    for (int i = 0; i < n; i++)
        for (int j = poz[i]; j < poz[i+1]; j++)
        {
            tab[1][k] = i;
            tab[0][k] = pred[j];
            k++;
        }
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cout << right << setw(4) << tab[i][j];
        }
        cout << "\n";
    }
    puts("-------------------------------");
}

//Conversie lista predecesori -> lista succesori
void lista_pred_to_lista_succ(int poz_pred[100], int pred[100], int n, int m)
{
    puts("\nLista succesori: ");
    int gr_ext[n + 1] = {0};
    int poz_succ[n+1];
    int succ[m];
    poz_succ[0] = 0;
    poz_succ[n] = m;
    int k = 0;
    for (int i = 0; i < m; i++)
    {
        gr_ext[pred[i]]++;                      //Se calculeaza gradul exterior al fiecarui nod   
    }
    for (int i = 1; i < n; i++)
    {                                                     //Se creeaza lista pozitiilor nodurilor
        poz_succ[i] = poz_succ[i - 1] + gr_ext[i - 1];    //Pozitia de inceput a predecesorilor lui i este gradul 
    }                                                     //nodului i + pozitia de inceput a pred. nod. i-1                                            
    for (int l = 0; l < n; l++)
    {
        for (int i = 0; i < m; i++)
        {
            if (pred[i] == l)                              //Se parcurge lista predecesorilor de n ori
            {                                              //La fiecare pas l se cauta succesorii 
                for (int j = 0; j < n; j++)                //nodului l. Daca in lista de pred se gaseste
                {                                          //nodul l, se cauta succesorul acestuia in  
                    if (poz_pred[j] <= i && i < poz_pred[j+1])       //in lista de pozitii
                    {
                        succ[k] = j;
                        k++;
                        break;
                    }
                }
            }
        }
    }
    cout << "\nnod:";
    for (int i = 0; i< n; i++)
        cout << right << setw(4) << i;
    cout << "\npoz:";
    for (int i = 0; i <= n; i++)
    {
        cout << right << setw(4) << poz_succ[i];
    }
    cout << "\npre:";
    for (int i = 0; i < m; i++)
    {
        cout << right << setw(4) << succ[i];
    }
    puts("\n-------------------------------");
}

//Afisare text meniu
void show_main_menu()
{
    cout << "\n------------Meniu--------------\n"
         << "1: Citire matrice adiacenta\n"
         << "2: Citire matrice incidenta\n"
         << "3: Citire lista arce\n"
         << "4: Citire lista succesori\n"
         << "5: Citire lista predecesori\n"
         << "6: Iesire"
         << "\n-------------------------------\n";
}

//Asteptare input in meniul principal
int get_input()
{
    int n, m;
    cout << "\n";
        int option;
        cin >> option;
        switch (option)
        {
            case 1: {int mat[100][100];
                    read_adiacenta(mat, n, m);
                    adiacenta_to_incidenta(mat, n, m);
                    adiacenta_to_lista_arce(mat, n, m);
                    adiacenta_to_lista_succ(mat, n, m);
                    adiacenta_to_lista_pred(mat, n, m);
                    break;}
            case 2: {int mat[100][100];
                    read_incidenta(mat, n, m);
                    incidenta_to_adiacenta(mat, n, m);
                    incidenta_to_lista_arce(mat, n, m);
                    incidenta_to_lista_succ(mat, n, m);
                    incidenta_to_lista_pred(mat, n, m);
                    break;}
            case 3: {int mat[2][100];
                    read_lista_arce(mat, n, m);
                    lista_arce_to_adiacenta(mat, n, m);
                    lista_arce_to_incidenta(mat, n, m);
                    lista_arce_to_lista_succ(mat, n, m);
                    lista_arce_to_lista_pred(mat, n, m);
                    break;}
            case 4: {int poz[100];
                    int succ[100];
                    read_lista_succ(poz, succ, n, m);
                    lista_succ_to_adiacenta(poz, succ, n, m);
                    lista_succ_to_incidenta(poz, succ, n, m);
                    lista_succ_to_lista_arce(poz, succ, n, m);
                    lista_succ_to_lista_pred(poz, succ, n, m);
                    break;}
            case 5: {int poz[100];
                    int pred[100];
                    read_lista_pred(poz, pred, n, m);
                    lista_pred_to_adiacenta(poz, pred, n, m);
                    lista_pred_to_incidenta(poz, pred, n, m);
                    lista_pred_to_lista_arce(poz, pred, n, m);
                    lista_pred_to_lista_succ(poz, pred, n, m);
                    break;}
            case 6: return 0; 
            default: cout << "Numar invalid!";
        }
    return 1;
}

int main()
{
    int cont = 1;
    while(cont){
        show_main_menu();
        cont = get_input();
    }    
}
